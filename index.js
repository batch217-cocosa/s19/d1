






// Conditional Statements
// If, else if, else clause



let numG = -1;


if (numG < 0){

	console.log("Hello");
}


if (numG < 0){

	console.log("Hello");
}


let numH = 1;

if (numG > 0){
	console.log("Hello");
}

else if (numH > 0){
	console.log("World")
}



if (numG > 0){
	console.log("Hello");
}

else if (numH = 0){
	console.log("World")
}

else {
	console.log("Again");

}



let message = "No message";
console.log(message);


function determinTyphoonIntensity(windSpeed){

	if (windSpeed < 30){
		return "Not a typhoon yet";
	}
	else if (windSpeed <= 61) {
		return "Tropical depression detected.";
	}
	else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected";
	}

	else if (windSpeed >= 89 && windSpeed <= 177){
		return "Sever tropical storm detected";
	}
	else{
		return "Typhoon detected";
	}
}


message = determinTyphoonIntensity(69);
console.log(message);


message = determinTyphoonIntensity(27);
console.log(message);

message = determinTyphoonIntensity(200);
console.log(message);

message = determinTyphoonIntensity(100);
console.log(message);


if (message == "Tropical storm detected"){
	console.warn(message);
}

message = determinTyphoonIntensity(75);
console.log(message);

if (message == "Tropical storm detected"){
	console.warn(message);
}


// Truthy and Falsy

if (true) {
	console.log("Truthy");
}

if (1) { //1 = true
	console.log("Truthy");
}

if ([]){ // true
	console.log("Truthy");
}

if (false) {
	console.log("Falsy");
}

if (0) { //0 == false
	console.log("Falsy");
}


if (undefined){
	console.log("Falsy");
}


// Conditional (Ternary) Operators

/*
	- The conditional (Ternary) Operator takes in three operands:

	1. condition
	2. expression to execute if the condition is true
	3. expression to execute if the condition is false
*/

// Single statement executon

let ternaryResult = (1 < 18) ? "Hello" : "World";
console.log("Result of Ternary Operator: " + ternaryResult);


// multiple statement execution

let name;

function isOfLegalAge(){

	name = "John";
	return "You are of legal age limit";
}


function isUnderAge() {
	name = "Jane";
	return "You are under the age limit";
}


let age = parseInt(prompt("What is your age?"));

//parseInt = converts a string number into an Integer 


console.log(age);


let legalAge = (age > 18) ? isUnderAge() : isUnderAge();

console.log("Result of ternary Operator in function: " + legalAge + ", " + name );




// Syntax

/*
switch (expression) {
	case value:
		statement;
		break;
	default:
		statement;
		break;
}

*/


let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);


switch (day){

	case "monday":
		console.log("The color of the day is red");
		break

	case "tuesday":
		console.log("The color of the day is orange");
		break

	case "wednesday":
		console.log("The color of the day is yellow");
		break

	case "thursday":
		console.log("The color of the day is green");
		break

	case "friday":
		console.log("The color of the day is blue");
		break

	case "saturday":
		console.log("The color of the day is indigo");
		break

	case "sunday":
		console.log("The color of the day is violet");
		break

	default:
		console.log("Please input a valid day");
}






// Try-Catch-Finally Statement


function showIntensityAlert(windSpeed) {
	try{
		// attempt to execute a code
		alert(determinTyphoonIntensity(windSpeed));

	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert");
	}
}

showIntensityAlert(56);